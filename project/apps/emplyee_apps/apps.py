from django.apps import AppConfig


class EmplyeeAppsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.emplyee_apps'
    verbose_name = 'Сотрудники'
