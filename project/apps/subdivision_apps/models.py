from django.contrib import messages
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from mptt.models import MPTTModel, TreeForeignKey


class Subdivision(MPTTModel):
    name = models.CharField(max_length=200, verbose_name='Наименование подразделения')
    parent = TreeForeignKey('self', verbose_name='ID родительского подраздеения', on_delete=models.CASCADE, related_name='children', blank=True, null=True)
    level = models.IntegerField(default=0, verbose_name='Уровень привязанности', validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ])

    class Meta:
        db_table = 'subdivision'
        verbose_name = 'Подразделение'
        verbose_name_plural = 'Подразделении'

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return f'{self.name}'


@receiver(pre_save, sender='subdivision_apps.Subdivision')
def add_lvl(**kwargs):
    instance = kwargs.get('instance')
    if instance.parent:
        parent = instance.parent
        parent = Subdivision.objects.get(id=parent.id)
        instance.level = parent.level + 1
        if instance.level > 5:
            instance.level = 0
            instance.parent = None
    else:
        instance.level = 0
