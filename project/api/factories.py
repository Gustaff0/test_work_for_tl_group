import datetime
import factory
import random

from apps.emplyee_apps.models import Employee
from apps.subdivision_apps.models import Subdivision


class SubdivisionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subdivision

    name = factory.Faker('name', locale='ru_RU')
    parent = factory.SubFactory('api.factories.SubdivisionFactory')


class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Employee

    full_name = factory.Faker('name', locale='ru_RU')
    position = factory.Faker('name', locale='ru_RU')
    date_accept = factory.LazyFunction(datetime.datetime.now)
    salary = factory.LazyAttribute(lambda x: random.randrange(10000, 80000))
    subdivision = factory.SubFactory(SubdivisionFactory)
