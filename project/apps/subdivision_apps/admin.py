from django import forms
from django.contrib import admin
from apps.subdivision_apps.models import Subdivision


@admin.register(Subdivision)
class SubdivisionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'parent']
    fields = ['id', 'name', 'parent', 'level']
    readonly_fields = ['id', 'level']


