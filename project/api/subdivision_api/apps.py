from django.apps import AppConfig


class SubdivisionApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.subdivision_api'
