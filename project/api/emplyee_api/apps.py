from django.apps import AppConfig


class EmplyeeApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.emplyee_api'
