from django.contrib import admin
from apps.emplyee_apps.models import Employee


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['id', 'full_name', 'position', 'date_accept']
    fields = ['id', 'full_name', 'position', 'date_accept', 'salary', 'subdivision']
    readonly_fields = ['id',]
