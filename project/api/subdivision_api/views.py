from django.shortcuts import render
from api.factories import SubdivisionFactory, EmployeeFactory
from apps.subdivision_apps.models import Subdivision


def show_genres(request):
    return render(request, "index.html", {'subdivisions': Subdivision.objects.all()})
