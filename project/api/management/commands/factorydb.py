from django.core.management.base import BaseCommand, CommandError
from api.factories import SubdivisionFactory, EmployeeFactory


class Command(BaseCommand):
    help = 'The create FactoryBoy'

    def handle(self, *args, **options):
        for i in range(1, 50001):
            EmployeeFactory(subdivision=SubdivisionFactory(parent__parent__parent__parent__parent__parent=None))
        self.stdout.write(self.style.SUCCESS('Successfully created'))
