from django.urls import path

from api.subdivision_api.views import show_genres

app_name = 'api.subdivision_api'

urlpatterns = [
    path('tree/', show_genres, name='tree'),
]
