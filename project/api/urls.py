from django.urls import path, include

app_name = 'api'

urlpatterns = [
    path('home/', include('api.subdivision_api.urls')),
]
