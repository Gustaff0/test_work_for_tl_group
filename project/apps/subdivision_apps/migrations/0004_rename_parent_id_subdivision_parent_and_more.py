# Generated by Django 4.1.3 on 2022-11-26 16:34

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subdivision_apps', '0003_alter_subdivision_options_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subdivision',
            old_name='parent_id',
            new_name='parent',
        ),
        migrations.AlterField(
            model_name='subdivision',
            name='level',
            field=models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(5), django.core.validators.MinValueValidator(0)], verbose_name='Уровень привязанности'),
        ),
    ]
