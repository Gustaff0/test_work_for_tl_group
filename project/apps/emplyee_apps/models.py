from django.db import models


class Employee(models.Model):
    full_name = models.CharField(max_length=300, verbose_name='ФИО')
    position = models.CharField(max_length=100, verbose_name='Должность')
    date_accept = models.DateTimeField(verbose_name='Дата приема на работу')
    salary = models.IntegerField(verbose_name='Размер Зароботной платы')
    subdivision = models.ForeignKey('subdivision_apps.Subdivision', verbose_name='Подразделение', on_delete=models.CASCADE, related_name='employees')

    class Meta:
        db_table = 'employee'
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'