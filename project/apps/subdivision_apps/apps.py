from django.apps import AppConfig


class SubdivisionAppsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.subdivision_apps'
    verbose_name = 'Подразделение'
